#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OS_LINUX 1
#define OS_WIN32 0

void say(char string[254]);

int sys(char cmd[254]);

/* gcc <source> -o <binary> -std=c99 */

/******************************************
  FUNCTIONS 
  The majority of the chars should be on 
  the "home row" of the keyboard for
  ergonomics.

  say - Akin to echo/print.
  sys - Execute a system command.
  cal - Perform a mathematic calculation.
******************************************/

int main()
{

	char c;
	char buffer[100] = {0};
	char line[50] = {0};
	char var_nme[50] = {0};
	int var_val = {0};
	unsigned int say_flag = 0;
	unsigned int sys_flag = 0;
	unsigned int buf_flag = 0;
	unsigned int var_flag = 0;
	unsigned int x = 0;
	unsigned int z = 0;
	/*char func_buf[1][1][1] = {0}; */ /* Store characters here for comparison e.g. [s][a][y], then clear buffer */
	/* app[LINE][FUNC][DATA]; */
	/* char user_vars[var] = value */

	while ((c = getchar()) != EOF) {
		if ((c == 's') && (say_flag == 0)) {
			say_flag = 1;
		} else if ((c == 'a') && (say_flag == 1)) {
			say_flag = 2;
		} else if ((c == 'y') && (say_flag == 2)) {
			say_flag = 3;
		}
		
		if (say_flag == 3) {
			if ((c == '\'') && (buf_flag == 0)) {
				buf_flag = 1;
			} else if ((buf_flag == 1) && (c != '\'')) {
				line[x++] = c;
			} else if ((c == '\'') && (buf_flag == 1)) {				
				unsigned int y = 0;
				/*
				while (y <= x) {
					putchar(line[y++]);
				}
				*/
				printf("%s", line);
				printf("\n");
				/* Use memset to "clear" the line array. */
				memset(&line[0], 0, sizeof(line));
				/* Reset x ,buf_flag & say_flag to 0 */
				x = 0;
				buf_flag = 0;
				say_flag = 0;
                                /* Reset sys_flag as well, since it will be set to 1 */
				sys_flag = 0;
			}
		} /* End if say_flag == 3 */
		if ((c == 's') && (sys_flag == 0)) {
			sys_flag = 1;
		} else if ((c == 'y') && (sys_flag == 1)) {
			sys_flag = 2;
		} else if ((c == 's') && (sys_flag == 2)) {
			sys_flag = 3;
		}
		if (sys_flag == 3) {
			if ((c == '\'') && (buf_flag == 0)) {
				buf_flag = 1;
			} else if ((buf_flag == 1) && (c != '\'')) {
				line[x++] = c;
			} else if ((c == '\'') && (buf_flag == 1)) {				
				unsigned int y = 0;
				/* No good loop?
				while (y <= x) {
					char sys[254] = line[y++];
				}
				*/
				/*printf("In the sys?\n");*/
				system("gedit &");
				/*char exec[254];*/
				/*sprintf(exec, "%s", sys);*/
				/*system(exec);*/
                                //printf("I'm in the sys func!\n");
				buf_flag = 0;
			}
		}
	} /* End while getchar() */
	/*say("testing the function");*/
}

void say(char string[254])
{
	printf("%s", string);
}

void say_gtk()
{
	/* placeholder */
}

void say_win32()
{
	/* placeholder */
}

int sys(char cmd[254])
{
	char exec[254];
	/* Start is Win32 only, it is to begin the process without hanging the entire binary. */
	char start[] = "start";
	sprintf(exec, "%s %s", start, cmd);
	system(exec);
	return 0;
}
